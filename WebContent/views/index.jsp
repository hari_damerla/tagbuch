<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/jquerycss.min.css">
<link rel="stylesheet" href="../css/registor.css">
<link rel="stylesheet" href="../css/milti-file-upload.css">

<title>Tagbuch</title>
</head>
<body data-ng-app="myapp">

<nav   class="navbar navbar-inverse">
  <div class="navbar-header">
      <a class="navbar-brand" href="#">WebSiteName</a>
    </div>
    
    <ul class="nav navbar-nav"  >
      <li class="active"><a href="#">Home</a></li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown"  href="#">Page 1
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Page 1-1</a></li>
          <li><a href="#">Page 1-2</a></li>
          <li><a href="#">Page 1-3</a></li>
        </ul>
        
      </li>
      <li><a href="#">Page 2</a></li>
      <li><a href="#">Page 3</a></li>
    </ul>
  
  <ul class="nav navbar-nav navbar-right">
      <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
      <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
    </ul>
</nav>


<div data-ng-view></div>
 

<script src="../scripts/angular.min.js"></script>
<script src="../scripts/jquery.min.js"></script>
<script src="../scripts/bootstrap.min.js"></script>
<script src="../scripts/angular-route.min.js"></script>
<script src="../scripts/jqueryui.min.js"></script>
<script src="../scripts/route.js"></script>
<script type="text/javascript"></script>
<script >
  $( function() {
    $( ".menu" ).menu();
  } );
  </script>
  <style>
  .ui-menu { hight: 250px; }
  </style>
</body>
</html>