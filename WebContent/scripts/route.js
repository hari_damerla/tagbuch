var app = angular.module("myapp", ["ngRoute"]);
app.config(function($routeProvider) {
    $routeProvider
    .when("/address", {
    	templateUrl : "../views/contact.jsp"
    })
    .when("/uploadimage", {
    	templateUrl : "../views/uploadImage.jsp"
    })
    .when("/login", {
        templateUrl : "../views/loginview.jsp"
    });
});