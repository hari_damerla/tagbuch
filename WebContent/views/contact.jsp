<select name="mapCat">
    <option value="opt0" selected="selected">SELECT A CATEGORY</option>
    <option value="opt1">UNIVERSITIES</option>
    <option value="opt2">HOSPITALS</option>
</select>
<div id="mapContent">
    <ul>
       <li class="opt1">University X</li>
       <li class="opt2">Hospital X</li>
       <li class="opt2">Hospital Y</li>
       <li class="opt1">University Y</li>
       <li class="opt1">University Z</li>
       <li class="opt2">Hospital Z</li>
    </ul>
</div>